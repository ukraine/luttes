.. index::
   ! Mriya

.. _mriya:

==========================================================
**mriya** |mriya|
==========================================================

- https://www.facebook.com/MriyaUkraineGrenoble
- https://www.instagram.com/mriya.ukraine.association/
- https://www.mriya-ukraine.org/


Tel: 06 78 08 82 04

.. toctree::
   :maxdepth: 6

