.. index::
   ! Brigades Editoriales de Solidarité

.. _bes:

================================================================
**Brigades Editoriales de Solidarité** |solidarite_ukraine|
================================================================

- :ref:`ukraine_media_2023:bes_2023`
- :ref:`ukraine_media_2022:bes_2022`

