
.. raw:: html

   <a rel="me" href="https://qoto.org/@iranluttes"></a>

.. ⚖️
.. 🔥
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳 unicef
.. 🇫🇷
.. 🇧🇪
.. 🇬🇧
.. 🇺🇸 🇨🇦
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️
.. 🇮🇷
.. 📣
.. 💃
.. 🎻

.. un·e

|FluxWeb| `RSS <https://ukraine.frama.io/luttes/rss.xml>`_

.. _ukraine_luttes:

==========================================================
**Luttes en Ukraine**  |solidarite_ukraine|
==========================================================

- https://en.wikipedia.org/wiki/Ukraine
- https://www.syllepse.net/en-telechargement-gratuit-_r_20.html
- https://fr.wikipedia.org/wiki/Histoire_des_Juifs_en_Ukraine
- https://www.mahj.org/fr/media/juifs-dukraine-une-memoire-en-peril

.. https://jguideeurope.org/fr/

.. figure:: images/mriya.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 6

   news/news
   organisations/organisations

